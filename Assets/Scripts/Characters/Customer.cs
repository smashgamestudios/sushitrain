﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Customer : MonoBehaviour {

    public enum State
    {
        ORDERING, EATING, SATISFIED, ANIMAT_IN, ANIMAT_OUT
    }

    public State myState = State.ORDERING;
    public float eatingTime;
    public float payTime;
    public float waitTime;
    public float animTime;
    public Sushi choice;

    [HideInInspector] public Seat seat;
    [HideInInspector] public Vector3 direction;
    [SerializeField] private GameObject choiceUI;

    float myTimer = 0;
    FoodCheck foodCheck;
    float maxAmount;
    private UI ui;

    [SerializeField]Canvas GUI;
    [SerializeField]Image timerUI;
    [HideInInspector]public bool inTouch;

    void Start()
    {
        ui = FindObjectOfType<UI>();
        GUI.gameObject.SetActive(false);
        myState = State.ANIMAT_IN;
        foodCheck = GetComponentInChildren<FoodCheck>();
        direction = seat.transform.position - seat.entry.transform.position;
        seat.occupied = true;
        choice = ui.sushiList[Random.Range(0, ui.sushiList.Length)];
        choiceUI.GetComponentsInChildren<SpriteRenderer>()[1].sprite = choice.GetComponent<SpriteRenderer>().sprite;

    }

    void Update()
    {
        FSM();
    }

    void FSM()
    {
        switch (myState)
        {
            case State.ANIMAT_IN:
                if (!inTouch)
                {
                    transform.Translate(direction*Time.deltaTime,Space.World);
                }
                else
                {
                    myTimer = waitTime;
                    maxAmount = myTimer;
                    myState = State.ORDERING;
                    choiceUI.SetActive(true);
                    StartCoroutine(runTimer());
                }
                break;
            case State.ORDERING:
                if (myTimer <= 0)
                {
                    choiceUI.SetActive(false);
                    myState = State.ANIMAT_OUT;
                }
                break;
            case State.EATING:
                if (myTimer <= 0)
                {
                    postEat();
                }
                break;
            case State.SATISFIED:
                if (myTimer <= 0)
                {
                    myState = State.ANIMAT_OUT;
                }
                break;
            case State.ANIMAT_OUT:
                    transform.Translate(-direction*Time.deltaTime, Space.World);
                break;
        }
    }

    public void arrived(GameObject food)
    {
        if (myState != State.ORDERING)
            return;

        choiceUI.SetActive(false);
        Destroy(food);
        myTimer = eatingTime;
        maxAmount = myTimer;
        myState = State.EATING;
        StartCoroutine(runTimer());
    }

    //Coroutines
    IEnumerator runTimer()
    {
        GUI.gameObject.SetActive(true);
        while (myTimer>0)
        {
            timerUI.fillAmount = myTimer / maxAmount;
            myTimer -= Time.deltaTime;
            yield return GameManager.wait0s;
        }
        GUI.gameObject.SetActive(false);
        yield return null;
    }

    void postEat()
    {
        foodCheck.spawnDirtyPlate();
        myTimer = payTime;
        maxAmount = myTimer;
        myState = State.SATISFIED;
        StartCoroutine(runTimer());
    }
}