﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Chef : MonoBehaviour {

    public enum State
    {
        IDLE, COOKING, HOLDING, WASHING, COLLECTING
    }

    #region variables
    public State myState = State.IDLE;
    public float myTimer;
    public float speed;

    Belt myBelt;
    Sushi toBeMade;
    Transform sink;
    UI ui;
    #endregion

    private bool reached = false;
    private Vector3 initPos;
    private bool holding = false;
    private WaitForSeconds cleaningTime = new WaitForSeconds(1);

    [SerializeField]private Image timerUI;
    [SerializeField]private Canvas UI;

    void Start()
    {
        cleaningTime = new WaitForSeconds(GameManager.cleanTime);
        initPos = transform.position;
        myBelt = GameObject.FindGameObjectWithTag("Belt").GetComponent<Belt>();
        sink = GameObject.FindGameObjectWithTag("Sink").transform;
        ui = GameObject.FindObjectOfType<UI>();
        UI.gameObject.SetActive(false);
    }

    void Update()
    {
        FSM();
    }

    void FSM()
    {
        switch (myState)
        {
            case State.COOKING:
                if (myTimer <= 0)
                {
                    Instantiate(toBeMade, myBelt.spawnPoint.position, Quaternion.identity);
                    myState = State.IDLE;
                }
                break;
        }
    }

    public void cook(Sushi s)
    {
        if (myState == State.IDLE)
        {
            toBeMade = s;
            myTimer = s.t;
            myState = State.COOKING;
            Debug.Log("Started Cooking " + s.name);
            StartCoroutine(runTimer());
        }
    }

    //Coroutines
    private IEnumerator runTimer()
    {
        UI.gameObject.SetActive(true);
        float maxAmount = myTimer;
        while (myTimer>0)
        {
            myTimer -= Time.deltaTime;
            timerUI.fillAmount = myTimer / maxAmount;
            yield return GameManager.wait0s;
        }
        UI.gameObject.SetActive(false);
        yield return null;
    }

    private IEnumerator WalkToCollect(Dirty d)
    {
        myState = State.HOLDING;
        while (Vector2.Distance(d.transform.position, transform.position) > 1f)
        {
            transform.position = Vector3.Lerp(transform.position, d.transform.position, Time.deltaTime * speed);
            yield return GameManager.wait0s;
        }
        Destroy(d.gameObject);
        holding = true;

        StartCoroutine(WalkToWash());
    }

    private IEnumerator WalkToEarn(Customer c)
    {
        myState = State.COLLECTING;
        while (Vector2.Distance(c.transform.position, transform.position) > 3f)
        {
            transform.position = Vector3.Lerp(transform.position, c.transform.position, Time.deltaTime * speed);
            yield return GameManager.wait0s;
        }

        ui.setExp(c.choice.cost);
        c.myState = Customer.State.ANIMAT_OUT;

        StartCoroutine(goBack());
    }

    private IEnumerator WalkToWash()
    {
        myState = State.WASHING;
        while (Vector2.Distance(sink.transform.position, transform.position) > 0.5f)
        {
            transform.position = Vector3.Lerp(transform.position, sink.transform.position, Time.deltaTime * speed);
            yield return GameManager.wait0s;
        }

        yield return cleaningTime;
        holding = false;

        StartCoroutine(goBack());
    }

    private IEnumerator goBack()
    {
        while (Vector2.Distance(transform.position, initPos) > 0.5f)
        {
            transform.position = Vector3.Lerp(transform.position, initPos, Time.deltaTime * speed);
            yield return GameManager.wait0s;
        }
        myState = State.IDLE;
    }

    //Public ClickListeners
    public void collect(Dirty d)
    {
        StartCoroutine(WalkToCollect(d));
    }

    public void earn(Customer c)
    {
        if(c.myState == Customer.State.SATISFIED)
            StartCoroutine(WalkToEarn(c));
    }
}