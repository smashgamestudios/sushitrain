﻿using UnityEngine;

public class Dirty : Sushi {

    bool nullCheck = false;

    void Update()
    {
        transform.Translate(transform.right*Time.deltaTime*2,Space.World);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("WP"))
        {
            direction = other.transform.rotation.eulerAngles;
            transform.rotation = Quaternion.Euler(direction);
        }

        if (other.CompareTag("Finish"))
        {
            if (nullCheck)
            {
                FindObjectOfType<UI>().setExp(-negExp);
                StartCoroutine(Delete());
            }

            nullCheck = !nullCheck;
        }

    }

}