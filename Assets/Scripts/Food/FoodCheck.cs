﻿using UnityEngine;

public class FoodCheck : MonoBehaviour {

    Customer c;
    [SerializeField]GameObject dirtyPlate;
    Vector3 direction;

    void Start()
    {
        c = GetComponentInParent<Customer>();
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Belt"))
        {
            c.inTouch = true;
        }

        if (other.CompareTag(c.choice.tag))
        {
            direction = other.GetComponent<Sushi>().direction;
            c.arrived(other.gameObject);
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Belt"))
        {
            c.inTouch = false;
        }

        if (other.CompareTag("Entry"))
        {
            dissappear();
        }
    }

    public void spawnDirtyPlate()
    {
        Dirty dp = Instantiate(dirtyPlate, transform.position, c.transform.rotation).GetComponent<Dirty>();
        dp.direction = direction;
    }

    void dissappear()
    {
        if (c.myState != Customer.State.ANIMAT_OUT)
            return;
        c.seat.occupied = false;
        Destroy(c.gameObject);
    }
}