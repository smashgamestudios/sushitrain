﻿using UnityEngine;
using System.Collections;

public class Sushi : MonoBehaviour {

    public float t;
    public float cost;
    [SerializeField]protected float negExp;
    public Vector3 direction;
    protected Transform bin;

    void Start()
    {
        direction = transform.rotation.eulerAngles;
        bin = GameObject.FindGameObjectWithTag("Bin").transform;
    }

    void Update()
    {
        transform.Translate(transform.right*Time.deltaTime*2,Space.World);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("WP"))
        {
            direction = other.transform.rotation.eulerAngles;
            transform.rotation = Quaternion.Euler(direction);
        }

        if (other.CompareTag("Finish"))
        {
            FindObjectOfType<UI>().setExp(-negExp);
            StartCoroutine(Delete());
        }
    }

    protected IEnumerator Delete()
    {
        while (Vector2.Distance(transform.position, bin.position) > 0.5f)
        {
            yield return GameManager.wait0s;
            transform.position = Vector2.Lerp(transform.position, bin.position, Time.deltaTime * 7.5f);
        }
        Destroy(gameObject);
    }
}