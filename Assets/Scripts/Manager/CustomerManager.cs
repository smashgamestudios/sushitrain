﻿using UnityEngine;
using System.Collections;

public class CustomerManager : MonoBehaviour {

    [SerializeField] Customer prefab;
    public Seat[] entries;

    private float timer = 0;

    void Update()
    {
        if(timer <= 0)
            calculateSpawn();
    }

    void calculateSpawn()
    {
        Seat x = null;
        foreach (Seat s in entries)
        {
            if (s.occupied)
                continue;
            x = s;
            break;
        }
        if (x != null)
        {
            Customer c = Instantiate(prefab, x.entry.transform.position, x.entry.transform.rotation);
            c.seat = x;
        }
        timer = Random.Range(5, 10);
        StartCoroutine(runTimer());
    }

    IEnumerator runTimer()
    {
        while (timer > 0)
        {
            timer -= Time.deltaTime;
            yield return GameManager.wait0s;
        }
        yield return null;
    }

}