﻿using UnityEngine;

public class TouchDetector : MonoBehaviour {

    Chef chef;
    Ray ray;
    RaycastHit2D hit;

    void Start()
    {
        chef = GameObject.FindGameObjectWithTag("Player").GetComponent<Chef>();
    }

	void Update () {
        detectTouch();   
	}

    void detectTouch()
    {
        if (Input.GetMouseButtonDown(0))
        {
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            hit = Physics2D.Raycast(ray.origin, ray.direction);
            if (hit)
            {
                if (hit.collider.CompareTag("Dirty"))
                {
                    if(chef.myState == Chef.State.IDLE || chef.myState == Chef.State.HOLDING)
                        clickDirty(hit.collider.GetComponent<Dirty>());
                }

                if (hit.collider.CompareTag("Customer"))
                {
                    if (chef.myState == Chef.State.IDLE)
                        clickCustomer(hit.collider.GetComponent<Customer>());
                }
            }
        }
    }

    void clickDirty(Dirty d)
    {
        chef.collect(d);
    }

    void clickCustomer(Customer c)
    {
        chef.earn(c);
    }
}
