﻿using UnityEngine;

public class GameManager : MonoBehaviour {

    public static GameManager instance;

    //Constants Class
    public static WaitForSeconds wait0s = new WaitForSeconds(0);
    public static float cleanTime = 0.5f;

    void Start()
    {
        instance = this;
    }

}