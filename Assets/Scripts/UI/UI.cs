﻿using UnityEngine;
using UnityEngine.UI;

public class UI : MonoBehaviour {

    Chef chef;

    public Sushi[] sushiList;
    [SerializeField] Slider progressBar;
    [SerializeField] float requiredExp;

    float exp;

    void Start()
    {
        chef = GameObject.FindGameObjectWithTag("Player").GetComponent<Chef>();
        exp = 0;
        setExp(0);
    }

    public void Cook(int x)
    {
        chef.cook(sushiList[x]);
    }

    public void setExp(float e)
    {
        exp += e;
        progressBar.value = exp / requiredExp;

        if(progressBar.value >= 1)
        {
            //Next Level
        }
    }

}